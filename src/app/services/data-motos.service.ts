import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataMotosService {

  listaMotos: any[]=[]

  constructor( private http: HttpClient) { }


  async getMotos(){
    const respuesta = await fetch("http://motos.puigverd.org/motos");
    this.listaMotos = await respuesta.json();
    //console.log('DATA',this.listaMotos);
    return this.listaMotos;
  }

  async getMotosFiltro(marca:string){
    const respuesta = await fetch('http://motos.puigverd.org/motos?marca='+marca);
    this.listaMotos = await respuesta.json();
    //console.log('DATA',this.listaMotos);
    return this.listaMotos;
  }

}
