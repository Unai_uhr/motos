import { Component } from '@angular/core';
import { DataMotosService } from '../services/data-motos.service';
import { FormsModule } from '@angular/forms';
import { MotoItem } from '../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    listaMotos :any;
    listaMotosPorMarca: any;




  constructor(
    private motoData: DataMotosService, 
    private router: Router,
    private menu: MenuController,
    ) {}

  ngOnInit():void{
    this.cargarListaMotos();
    console.log("oooooooooo")
  }

  toggleMenu(){
    this.menu.toggle();
  }

  ionViewWillEnter() {
    this.cargarListaMotos();
    console.log("AAAAAAAAA")
  }


  async cargarListaMotos(event?){
    this.listaMotos=await this.motoData.getMotos();

  }

  

  abritDetalle(moto:MotoItem){
    console.log('click',moto)
    let navigationExtras: NavigationExtras = {
      state: {
        motoItem: moto,
      }
    };
    this.router.navigate(['detalle-moto'], navigationExtras);
  }

  

}
