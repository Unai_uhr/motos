
export interface MotoItem{
      id: number;
      marca: string;
      year: string;
      modelo: string;
      foto: string;
      precio: string;
}
