import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { HomePage } from 'src/app/home/home.page';
import { DataMotosService } from 'src/app/services/data-motos.service';
import { MotoItem } from '../../interfaces/interfaces';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  listaFiltrada:any[]=[];

  constructor(
    private motoData: DataMotosService,
    private router: Router,
  ) { }

  ngOnInit() {}

  async filtrarMarca(marca:string){
    if(marca!="todas"){

      let navigationExtras: NavigationExtras = {
        state: {
          marcaMoto: marca,
        }
      };
      this.router.navigate(['home'], navigationExtras);

      //this.listaFiltrada=await this.motoData.getMotosFiltro(marca);
      //HomePage.listarFiltradas(this.listaFiltrada);
    }

  }

}
