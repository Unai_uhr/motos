import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleMotoPage } from './detalle-moto.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleMotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleMotoPageRoutingModule {}
